#pragma once
#include <iostream>

using namespace std;

struct my_stack {
	int data;
	my_stack *next;
};
void push(my_stack *&stack, int new_element);
void pop(my_stack *&stack);
int top(my_stack *stack);
void reverse(my_stack *&stack);
void write(my_stack *&stack);
void copy(my_stack *from, my_stack *&to);