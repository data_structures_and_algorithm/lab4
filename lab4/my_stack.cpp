#include "my_stack.h"

void push(my_stack *&stack, int new_element)
{
	my_stack *p = new my_stack;
	p->data = new_element;
	p->next = stack;
	stack = p;
}
void pop(my_stack *&stack)
{
	if (!stack)
		return;
	my_stack *p = stack;
	stack = stack->next;
	delete p;
}
int top(my_stack *stack)
{
	return stack->data;
}
void reverse(my_stack *&stack)
{
	if (!stack)
		return;
	my_stack *p = 0, *w;
	while (stack) {
		w = new my_stack;
		w->data = stack->data;
		w->next = p;
		p = w;
		w = stack;
		stack = stack->next;
		delete w;
	}
	stack = p;
}
void write(my_stack *&stack)
{
	reverse(stack);
	int data;
	my_stack *p = 0, *w;
	while (stack) {
		data = stack->data;
		cout << data << " ";
		w = new my_stack;
		w->data = data;
		w->next = p;
		p = w;
		w = stack;
		stack = stack->next;
		delete w;
	}
	stack = p;
	cout << endl;
}
void copy(my_stack *from, my_stack *&to)
{
	if (!from) {
		to = 0;
		return;
	}
	my_stack *p = from, *w = 0, *pw;
	while (p) {
		pw = new my_stack;
		pw->data = p->data;
		pw->next = w;
		w = pw;
		p = p->next;
	}
	to = w;
}