#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "my_stack.h"
#include <set>
#include <algorithm>
#include <vector>

using namespace std;

struct adjacency_matrix 
{
	int n;
	int **matrix;
};
struct incidence_matrix 
{
	int n, m;
	int **matrix;
};
struct edge_arc_set 
{
	int edge, arc;
	int **edge_matrix, **arc_matrix;
};
struct list {
	int data;
	list *next;
};
struct links_list 
{
	list *list;
	links_list *next;
};
struct graph
{
	adjacency_matrix *adjacency;
	incidence_matrix *incidence;
	edge_arc_set *set;
	links_list *list;
};
void clear(adjacency_matrix *adj)
{

}
void clear(incidence_matrix *inc)
{

}
void clear(edge_arc_set *set)
{

}
void clear(links_list *list)
{

}
void ini(adjacency_matrix *&m, int n) {
	m = new adjacency_matrix;
	m->n = n;
	m->matrix = new int*[n];
	for (int i = 0; i < n; ++i)
		m->matrix[i] = new int[n];
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			m->matrix[i][j] = 0;
}
void create(adjacency_matrix *&matrix, string s = "adjacency.txt")
{
	ifstream in(s);
	if (!in.is_open()) {
		return;
	}
	matrix = new adjacency_matrix;
	int n;
	in >> n;
	matrix->n = n;
	matrix->matrix = new int*[n];
	for (int i = 0; i < n; ++i)
		matrix->matrix[i] = new int[n];
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			in >> matrix->matrix[i][j];
}
void create(edge_arc_set *&set, string s = "set.txt")
{
	ifstream in(s);
	if (!in.is_open()) {
		return;
	}
	set = new edge_arc_set;
	int m;
	in >> m;
	set->edge = m;
	set->edge_matrix = new int *[m];
	for (int i = 0; i < m; ++i)
		set->edge_matrix[i] = new int[2];
	for (int i = 0; i < m; ++i)
		in >> set->edge_matrix[i][0] >> set->edge_matrix[i][1];
}
void create(incidence_matrix *&im, string s = "incidence.txt")
{
	ifstream in(s);
	if (!in.is_open()) {
		return;
	}
	im = new incidence_matrix;
	int n, m;
	in >> n >> m;
	im->m = m; im->n = n;
	im->matrix = new int*[n];
	for (int i = 0; i < n; ++i)
		im->matrix[i] = new int[m];
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < m; ++j)
			in >> im->matrix[i][j];
}
void create(links_list *&ll, string s = "list.txt")
{
	ifstream in(s);
	list *lp;
	if (!in.is_open()) {
		return;
	}
	string input;
	getline(in, input, '\n');
	istringstream is(input);
	links_list *p = ll;
	ll = new links_list;
	ll->list = new list;
	lp = ll->list;
	is >> ll->list->data;
	while (!is.eof()) {
		lp->next = new list;
		lp = lp->next;
		is >> lp->data;
	}
	lp->next = 0;
	p = ll;
	while (!in.eof()) {
		getline(in, input, '\n');
		istringstream is(input);
		p->next = new links_list;
		p = p->next;
		p->list = new list;
		lp = p->list;
		is >> lp->data;
		while (!is.eof()) {
			lp->next = new list;
			lp = lp->next;
			is >> lp->data;
		}
		lp->next = 0;
	}
	p->next = 0;
}
void write(adjacency_matrix *matrix)
{
	if (!matrix)
		return;
	int n = matrix->n;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j)
			cout << matrix->matrix[i][j] << " ";
		cout << endl;
	}
}
void write(incidence_matrix *matrix)
{
	if (!matrix)
		return;
	for (int i = 0; i < matrix->n; ++i) {
		for (int j = 0; j < matrix->m; ++j)
			cout << matrix->matrix[i][j] << " ";
		cout << endl;
	}
}
void write(edge_arc_set *set)
{
	if (!set)
		return;
	for (int i = 0; i < set->edge; ++i)
		cout << set->edge_matrix[i][0] << " "<< set->edge_matrix[i][1] << endl;		
	for (int i = 0; i < set->arc; ++i)
		cout << set->arc_matrix[i][0] << " " << set->arc_matrix[i][1] << endl;
}
void write(links_list *links_list)
{
	if (!links_list)
		return;
	while (links_list) {
		list *p = links_list->list;
		while (p) {
			cout << p->data << " ";
			p = p->next;
		}
		links_list = links_list->next;
		cout << endl;
	}
}
void write(graph *graph)
{
	write(graph->adjacency);
	write(graph->incidence);
	write(graph->list);
	write(graph->set);
}
incidence_matrix *adjacency_to_incidence(adjacency_matrix *matrix)
{
	int m = 0;
	for (int i = 0; i < matrix->n; ++i)
		for (int j = i; j < matrix->n; ++j)
			if (matrix->matrix[i][j] || matrix->matrix[j][i]) ++m;
	incidence_matrix *im = new incidence_matrix;
	im->m = m; im->n = matrix->n;
	im->matrix = new int*[matrix->n];
	for (int i = 0; i < matrix->n; ++i)
		im->matrix[i] = new int[m];
	for (int i = 0; i < matrix->n; ++i)
		for (int j = 0; j < m; ++j)
			im->matrix[i][j] = 0;
	m = 0;
	for (int i = 0; i < matrix->n; ++i)
		for (int j = i; j < matrix->n; ++j) {
			if (matrix->matrix[i][j] && matrix->matrix[j][i]) {
				im->matrix[i][m] = 1;
				im->matrix[j][m] = 1;
				++m;
				continue;
			}
			if (matrix->matrix[i][j]) {
				im->matrix[i][m] = 1;
				im->matrix[j][m] = -1;
				++m;
				continue;
			}
			if (matrix->matrix[j][i]) {
				im->matrix[i][m] = -1;
				im->matrix[j][m] = 1;
				++m;
				continue;
			}
		}
	return im;
}
edge_arc_set *adjacency_to_set(adjacency_matrix *matrix) {
	edge_arc_set *es = new edge_arc_set;
	int edge = 0, arc = 0;
	for (int i = 0; i <matrix->n; ++i)
		for (int j = i; j < matrix->n; ++j) {
			if (matrix->matrix[i][j] && matrix->matrix[j][i]) {
				++edge;
				continue;
			}
			if (!matrix->matrix[i][j] != !matrix->matrix[j][i]) {
				++arc;
				continue;
			}
		}
	es->arc = arc;
	es->edge = edge;
	es->arc_matrix = new int*[arc];
	for (int i = 0; i < arc; ++i)
		es->arc_matrix[i] = new int[2];
	es->edge_matrix = new int*[edge];
	for (int i = 0; i < edge; ++i)
		es->edge_matrix[i] = new int[2];
	edge = 0; arc = 0;
	for (int i = 0; i < matrix->n; ++i)
		for (int j = i; j < matrix->n; ++j) {
			if (matrix->matrix[i][j] && matrix->matrix[j][i]) {
				es->edge_matrix[edge][0] = i;
				es->edge_matrix[edge][1] = j;
				++edge;
				continue;
			}
			if (matrix->matrix[i][j]) {
				es->arc_matrix[arc][0] = i;
				es->arc_matrix[arc][1] = j;
				++arc;
				continue;
			}
			if (matrix->matrix[j][i]) {
				es->arc_matrix[arc][0] = i;
				es->arc_matrix[arc][1] = j;
				++arc;
				continue;
			}
		}
	return es;
}
links_list *adjacency_to_list(adjacency_matrix *matrix)
{
	links_list *l = 0, *p;
	list *list_pointer;
	l = new links_list;
	p = l;
	l->list = new list;
	l->list->next = 0;
	l->list->data = 0;
	for (int i = 1; i < matrix->n; ++i) {
		p->next = new links_list;
		p = p->next;
		p->list = new list;
		p->list->next = 0;
		p->list->data = i;
	}
	p->next = 0;
	p = l;
	for (int i = 0; i < matrix->n; ++i) {
		list_pointer = p->list;
		for (int j = 0; j < matrix->n; ++j) {
				if (matrix->matrix[i][j]) {
					list_pointer->next = new list;
					list_pointer = list_pointer->next;
					list_pointer->data = j;
				}
			}
		list_pointer->next = 0;
		p = p->next;
	}
	return l;
}
adjacency_matrix *list_to_adjacency(links_list *ll)
{
	adjacency_matrix *matrix = new adjacency_matrix;
	links_list *l = ll;
	int n = 0;
	while (l) {
		++n;
		l = l->next;
	}
	matrix->n = n;
	matrix->matrix = new int*[n];
	for (int i = 0; i < n; ++i)
		matrix->matrix[i] = new int[n];
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			matrix->matrix[i][j] = 0;
	for (int i = 0; i < n; ++i) {
		list *p = ll->list->next;
		while (p) {
			matrix->matrix[i][p->data] = 1;
			p = p->next;
		}
		ll = ll->next;
	}
	return matrix;
}
adjacency_matrix *incidence_to_adjacency(incidence_matrix *im)
{
	adjacency_matrix *matrix = new adjacency_matrix;
	int n = im->n;
	matrix->n = n;
	matrix->matrix = new int*[n];
	for (int i = 0; i < n; ++i)
		matrix->matrix[i] = new int[n];
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			matrix->matrix[i][j] = 0;
	for (int i = 0; i < im->m; ++i) {
		int a = -1, b = -1;
		for (int j = 0; j < n; ++j) {
			if (im->matrix[j][i]) {
				if (a == -1) a = j;
				else {
					b = j;
					break;
				}
			}
		}
		if (b == -1) b = a;
		if (im->matrix[a][i] == im->matrix[b][i]) {
			matrix->matrix[a][b] = 1;
			matrix->matrix[b][a] = 1;
			continue;
		}
		if (im->matrix[a][i] == -1) {
			matrix->matrix[b][a] = 1;
			continue;
		}
		matrix->matrix[a][b] = 1;

	}
	return matrix;
}
adjacency_matrix *set_to_adjacency(edge_arc_set *eas)
{
	adjacency_matrix *matrix = new adjacency_matrix;
	bool *ar = new bool[2 * (eas->arc + eas->edge)];
	for (int i = 0; i < 2 * (eas->arc + eas->edge); ++i) ar[i] = 0;
	for (int i = 0; i < eas->edge; ++i) {
		ar[eas->edge_matrix[i][0]] = 1;
		ar[eas->edge_matrix[i][1]] = 1;
	}
	for (int i = 0; i < eas->arc; ++i) {
		ar[eas->arc_matrix[i][0]] = 1;
		ar[eas->arc_matrix[i][1]] = 1;
	}
	int n = 0;
	for (int i = 0; i < 2 * (eas->arc + eas->edge); ++i) n+=ar[i];
	matrix->n = n;
	matrix->matrix = new int*[n];
	for (int i = 0; i < n; ++i)
		matrix->matrix[i] = new int[n];
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			matrix->matrix[i][j] = 0;
	for (int i = 0; i < eas->edge; ++i) {
		int a = eas->edge_matrix[i][0], b = eas->edge_matrix[i][1];
		matrix->matrix[a][b] = 1;
		matrix->matrix[b][a] = 1;
	}
	for (int i = 0; i < eas->arc; ++i) {
		int a = eas->arc_matrix[i][0], b = eas->arc_matrix[i][1];
		matrix->matrix[a][b] = 1;
	}
	return matrix;
}
void writeAdjGraph(graph *&g, string s = "")
{
	if (!g) {
		g = new graph;
		g->incidence = 0;
		g->set = 0;
		g->list = 0;
		g->adjacency = 0;
		create(g->adjacency);
		if (s.length() != 0)
			create(g->adjacency, s);
		else
			create(g->adjacency);
		return;
	}
	clear(g->incidence);
	g->incidence = 0;
	clear( g->set);
	g->set = 0;
	clear( g->list);
	g->list = 0;
	if (s.length() != 0)
		create(g->adjacency, s);
	else
		create(g->adjacency);
}
void writeIncGraph(graph *&g, string s = "")
{
	if (!g) {
		g = new graph;
		g->incidence = 0;
		g->set = 0;
		g->list = 0;
		g->adjacency = 0;
		create(g->adjacency);
		if (s.length() != 0)
			create(g->incidence, s);
		else
			create(g->incidence);
		return;
	}
	clear( g->set);
	g->set = 0;
	clear( g->list);
	g->list = 0;
	clear( g->adjacency);
	g->adjacency = 0;
	if (s.length() != 0)
		create(g->incidence, s);
	else
		create(g->incidence);
}
void writeSetGraph(graph *&g, string s = "")
{
	if (!g) {
		g = new graph;
		g->incidence = 0;
		g->set = 0;
		g->list = 0;
		g->adjacency = 0;
		create(g->adjacency);
		if (s.length() != 0)
			create(g->set, s);
		else
			create(g->set);
		return;
	}
	clear( g->incidence );
	g->incidence = 0;
	clear( g->list);
	g->list = 0;
	clear( g->adjacency);
	g->adjacency = 0;
	if (s.length() != 0)
		create(g->set, s);
	else
		create(g->set);
}
void writeListGraph(graph *&g, string s = "")
{
	if (!g) {
		g = new graph;
		g->incidence = 0;
		g->set = 0;
		g->list = 0;
		g->adjacency = 0;
		create(g->adjacency);
		if (s.length() != 0)
			create(g->list, s);
		else
			create(g->list);
		return;
	}
	clear( g->incidence);
	g->incidence = 0;
	clear( g->set);
	g->set = 0;
	clear( g->adjacency);
	g->adjacency = 0;
	if (s.length() != 0)
		create(g->list, s);
	else
		create(g->list);
}
void toAdj(graph *g)
{
	if (g->adjacency)
		return;
	if (g->incidence) {
		g->adjacency = incidence_to_adjacency(g->incidence);
		clear(g->incidence);
		g->incidence = 0;
	}
	if (g->set) {
		g->adjacency = set_to_adjacency(g->set);
		clear(g->set);
		g->set = 0;
	}
	if (g->list) {
		g->adjacency = list_to_adjacency(g->list);
		clear(g->list);
		g->list = 0;
	}
}
void toInc(graph *graph)
{
	if (graph->incidence)
		return;
	toAdj(graph);
	graph->incidence = adjacency_to_incidence(graph->adjacency);
	clear(graph->adjacency);
	graph->adjacency = 0;
}
void toSet(graph *graph)
{
	if (graph->set)
		return;
	toAdj(graph);
	graph->set = adjacency_to_set(graph->adjacency);
	clear(graph->adjacency);
	graph->adjacency = 0;
}
void toList(graph *graph)
{
	if (graph->list)
		return;
	toAdj(graph);
	graph->list = adjacency_to_list(graph->adjacency);
	clear(graph->adjacency);
	graph->adjacency = 0;
}
void writeMenu()
{
	cout << "������� ���� ���������"		<< 1;
	cout << "������� ���� �������������"	<< 2;
	cout << "������� ��������� �����"		<< 3;
	cout << "������� ������ ������"			<< 4;
	cout << "������� ����"					<< 5;
	cout << "�������� ������� ���������"	<< 6;
	cout << "�������� �������������"		<< 7;
	cout << "�������� ��������� �����"		<< 8;
	cout << "�������� ������ ������"		<< 9;
}
int min(int a, int b) {
	return (a < b) ? a : b;
}
void EulerianPath(adjacency_matrix *adj)
{
	if (!adj)
		return;
	int n = adj->n;
	int ccount = 0;
	int firstEl = 0;
	for (int i = 0; i < n; ++i) {
		int count = 0;
		for (int j = 0; j < n; ++j)
			count += adj->matrix[i][j];
		if (count % 2) {
			ccount++;
			firstEl = i;
			if (ccount > 2) {
				cout << "Eularian path doesn't exist\n";
				return;
			}
		}
	}
	int **matrix = new int*[n];
	for (int i = 0; i < n; ++i)
		matrix[i] = new int[n];
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			matrix[i][j] = adj->matrix[i][j];
	my_stack *res = 0, *ver = 0;
	push(ver, firstEl);
	int k;
	while (ver) {
		k = top(ver);
		int i;
		for (i = 0; i < n; ++i)
			if (matrix[k][i]) {
				matrix[i][k] = 0;
				matrix[k][i] = 0;
				push(ver, i);
				break;
			}
		//if (i != n) {
		//	matrix[i][k] = 0;
		//	matrix[k][i] = 0;
		//	push(ver, i);
		//}
		if (i == n) {
			pop(ver);
			push(res, k);
		}
	}
	write(res);
}
void Dijkstra(int *length, my_stack **&path, int v, adjacency_matrix *adj)
{
	if (v >= adj->n)
		return;
	int n = adj->n, sum = 0;
	bool *U = new bool[n];
	path = new my_stack*[n];
	for (int i = 0; i < n; ++i) {
		length[i] = ((1 << 31)-1);
		U[i] = 1;
		path[i] = 0;
	}
	length[v] = 0;
	while (sum < n) {
		int min = -1;
		for (int i = 0; i < n; ++i)
			if (U[i] && (length[i] < length[min] || min == -1)) min = i;
		U[min] = 0;
		++sum;
		for (int i = 0; i < n; ++i)
			if (U[i] && adj->matrix[min][i] && length[i] > length[min] + adj->matrix[min][i]) {
				length[i] = length[min] + adj->matrix[min][i];
				while (path[i]) pop(path[i]);
				path[i] = 0;
				copy(path[min], path[i]);
				reverse(path[i]);
				push(path[i], i);
			}
	}
}

void Floyd(adjacency_matrix *adj, int **D)
{
	int n = adj->n;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			if (adj->matrix[i][j] || i == j)
				D[i][j] = adj->matrix[i][j];
			else
				D[i][j] = (1 << 30) - 1;
			//cout << D[i][j] << " ";
		}
		//cout << endl;
	}
	for (int m = 0; m < n; ++m) {
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				D[i][j] = min(D[i][j], D[i][m] + D[m][j]);
			}
		}
	}
}
void Prim_work(adjacency_matrix *adj) {
	int n = adj->n;
	set<int> SP = {}, SM = { 0,1,2,3,4,5,6,7,8,9 };

	int min = (1 << 31) - 1;
	int l, t;

	for (int i = 0; i < n; ++i)
		for (int j = i; j < n; ++j)
			if (adj->matrix[i][j] < min && adj->matrix[i][j])
			{
				min = adj->matrix[i][j];
				l = i;
				t = j;
			}
	cout << "[" << l << ", " << t << " ];  ";
	SP.insert(l);   SM.erase(l);
	SP.insert(t);   SM.erase(t);
	while (!SM.empty())
	{
		min = (1 << 31) - 1;
		l = t = 0;
		for (auto i : SM)
			for (auto j : SP)
				if (adj->matrix[i][j] < min && adj->matrix[i][j] != 0)
				{
					min = adj->matrix[i][j];
					l = i;
					t = j;
				}
		SP.insert(l);   SM.erase(l);
		cout << "[" << l << ", " << t << " ];  ";
	}
}
void Prim(adjacency_matrix *adj)
{
	const int INF = 2147483647;
	int n = adj->n;
	bool *SM = new bool[n];
	int sumSM = n;
	int *key = new int[n], *p = new int[n];
	for (int i = 0; i < n; ++i) {
		key[i] = INF;
		p[i] = -1;
		SM[i] = 1;
	}
	key[0] = 0;
	int min = INF, v;
	for (int i = 0; i < n; ++i) {
		//cout << min << " " << key[i] << endl;
		if (SM[i] && min > key[i]) {
			min = key[i];
			v = i;
		}
	}
	SM[v] = 0;
	--sumSM;
	while (sumSM) {
		for (int i = 0; i < n; ++i) {
			if (SM[i] && adj->matrix[v][i] && adj->matrix[v][i] < key[i]) {
				key[i] = adj->matrix[v][i];
				p[i] = v;
			}
		}
		min = INF;
		for (int i = 0; i < n; ++i)
			if (SM[i] && min > key[i]) {
				min = key[i];
				v = i;
			}
		SM[v] = 0;
		--sumSM;
	}
	for (int i = 0; i < n; ++i)
		if (p[i] != -1)
			cout << i << " " << p[i] << endl;
}
void Dantzig(adjacency_matrix *adj, adjacency_matrix *m)
{
	int n = adj->n;
	int **D = new int*[n], **D0 = new int*[n];
	for (int i = 0; i < n; ++i) {
		D[i] = new int[n];
		D0[i] = new int[n];
	}
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j) {
			if (adj->matrix[i][j] || i == j)
				D[i][j] = adj->matrix[i][j];
			else
				D[i][j] = (1 << 30) - 2;
			D0[i][j] = D[i][j];
		}
	//for (int i = 0; i < n; ++i) {
	//	for (int j = 0; j < n; ++j)
	//		cout << D[i][j] << " ";
	//	cout << endl;
	//}

	for (int m = 0; m < n ; ++m) {
		for (int i = 0; i < m; ++i) {
			for (int j = 0; j < m; ++j) {
				D[m][j] = min(D[m][j], D0[m][i] + D[i][j]);
			}
		}
		for (int i = 0; i < m; ++i) {
			for (int j = 0; j < m; ++j) {
				D[i][m] = min(D[i][m], D[i][j] + D0[j][m]);
			}
		}
		for (int i = 0; i < m; ++i) {
			for (int j = 0; j < m; ++j) {
				D[i][j] = min(D[i][j], D[i][m] + D[m][j]);
			}
		}
	}

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			cout << D[i][j] << " ";
			m->matrix[i][j] = D[i][j];
		}
		cout << endl;
	}
}
void show_vector(vector<int>&a)
{
	for (vector<int>::iterator it = a.begin(); it != a.end(); ++it)
		cout << *it << " ";
	cout << endl;
}
void Kruskal(int **adj_matrix, int n) {
	//
	//int m;
	//vector < pair < int, pair<int, int> > > g(m); // ��� - ������� 1 - ������� 2
	//
	//int cost = 0;
	//vector < pair<int, int> > res;
	//
	//sort(g.begin(), g.end());
	//vector<int> tree_id(n);
	//for (int i = 0; i < n; ++i)
	//	tree_id[i] = i;
	//for (int i = 0; i < m; ++i)
	//{
	//	int a = g[i].second.first, b = g[i].second.second, l = g[i].first;
	//	if (tree_id[a] != tree_id[b])
	//	{
	//		cost += l;
	//		res.push_back(make_pair(a, b));
	//		int old_id = tree_id[b], new_id = tree_id[a];
	//		for (int j = 0; j < n; ++j)
	//			if (tree_id[j] == old_id)
	//				tree_id[j] = new_id;
	//	}
	//}
	//cout << cost << endl;
	//
	vector<vector<int>> edjes{};
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			if (adj_matrix[i][j]) {
				vector<int> v(3);
				v[0] = adj_matrix[i][j];
				v[1] = i;
				v[2] = j;
				edjes.push_back(v);
			}
		}
	}
	sort(edjes.begin(), edjes.end());

	vector <int> comp(n);
	vector<vector<int>> ok_edjes{};
	for (int i = 0; i < n; ++i)
		comp[i] = i;
	int count = 0;
	int weidth, first, last;
	for (int i = 0; i < edjes.size(); ++i) {
		weidth = edjes[i][0];
		first = edjes[i][1];
		last = edjes[i][2];
		if (comp[first] != comp[last]) {
			//comp[last] = comp[first];
			count += weidth;
			vector<int> v(2);
			v[0] = first;
			v[1] = last;
			ok_edjes.push_back(v);
			for (int j = 0; j < n; ++j) {
				if (comp[j] == comp[last] && j != last)
					comp[j] = comp[first];
			}
			comp[last] = comp[first];
		}
	}
	cout << count << endl;
	for (int i = 0; i < ok_edjes.size(); ++i) {
		show_vector(ok_edjes[i]);
	}

}
int main()
{
	graph *g = 0;
	my_stack **path = 0;
	adjacency_matrix *m1 = 0, *m2 = 0;
	int t = 21; //1 - 21, 15 - 10
	ini(m1, t);
	ini(m2, t);
	int num = -1;
	while (num) {
		cin >> num;
		switch (num)
		{
			case 1: {
				writeAdjGraph(g);
				break;
			}
			case 2: {
				writeIncGraph(g);
				break;
			}
			case 3: {
				writeSetGraph(g);
				break;
			}
			case 4: {
				writeListGraph(g);
				break;
			}
			case 5: {
				write(g);
				break;
			}
			case 6: {
				toAdj(g);
				break;
			}
			case 7: {
				toInc(g);
				break;
			}
			case 8: {
				toSet(g);
				break;
			}
			case 9: {
				toList(g);
				break;
			}
			case 10: {
				writeAdjGraph(g, "Eularian.txt");
				break;
			}
			case 11: {
				EulerianPath(g->adjacency);
				break;
			}
			case 90: {
				Kruskal(g->adjacency->matrix, g->adjacency->n);
				break;
			}
			case 12: {
				int n;
				cin >> n;
				int *length = new int[g->adjacency->n];
				Dijkstra(length, path, n, g->adjacency);
				for (int i = 0; i < g->adjacency->n; ++i)
					cout << i << '\t';
				cout << endl;
				for (int i = 0; i < g->adjacency->n; ++i)
					cout << length[i] << '\t';
				cout << endl;
				break;
			}
			case 13: {
				int n;
				cin >> n;
				write(path[n]);
				break;
			}
			case 14: {
				int n = g->adjacency->n;
				int **D = new int*[n];
				for (int i = 0; i < n; ++i)
					D[i] = new int[n];
				Floyd(g->adjacency, D);
				for (int i = 0; i < n; ++i) {
					for (int j = 0; j < n; ++j) {
						cout << D[i][j] << " ";
						m1->matrix[i][j] = D[i][j];
					}
					cout << endl;
				}
				break;
			}
			case 15: {
				writeAdjGraph(g, "weighted.txt");
				break;
			}
			case 16: {
				Prim(g->adjacency);
				break;
			}
			case 17: {
				Dantzig(g->adjacency, m2);
				break;
			}
			case 18: {
				bool b = 0;
				//m1->matrix[0][0] = 1;
				for (int i = 0; i < m2->n; ++i) {
					for (int j = 0; j < m2->n; ++j)
						if (m1->matrix[i][j] != m2->matrix[i][j]) {
							b = 1;
							break;
						}
					if (b) {
						cout << "\nFALSE\n";
						break;
					}
				}
			}
		}
	}
	system("pause");
	return 0;
}